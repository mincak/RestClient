#ifndef REQUEST_H
#define REQUEST_H

#include <QObject>
#include <QNetworkAccessManager>
#include "Response.h"

class Request : public QObject
{
    Q_OBJECT
public:
    explicit Request(QObject *parent = nullptr);
    void run(const QString &type, QString url, const QMap<QString, QString> &inputMap = QMap<QString, QString>());

signals:
    void response(Response *);

private slots:
    void requestFinished(QNetworkReply *reply);

private:
    QString inputMapToByteArray(const QMap<QString, QString> &inputMap);

private:
    QNetworkAccessManager *_mgr = nullptr;
    Response *_response = nullptr;
};

#endif // REQUEST_H
