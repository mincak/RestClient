#include "Response.h"
#include <QDebug>
#include <QPixmap>

Response::Response(QNetworkReply *reply, QObject *parent) : QObject(parent)
{
    _reply = reply;

    if(!hasError())
        _data = _reply->readAll();
}

bool Response::hasError()
{
    QNetworkReply::NetworkError errorType = _reply->error();
    return (errorType != QNetworkReply::NoError);
}

QString Response::error()
{
    return hasError() ? _reply->errorString() : "No error";
}

QByteArray Response::data()
{
    return _data;
}

QStringList Response::headers()
{
    QStringList list;
    if(!hasError()) {
        list << QString("ContentTypeHeader: " + _reply->header(QNetworkRequest::ContentTypeHeader).toString());
        list << QString("ContentLengthHeader: " + _reply->header(QNetworkRequest::ContentLengthHeader).toString());
        list << QString("LocationHeader: " + _reply->header(QNetworkRequest::LocationHeader).toString());
        list << QString("LastModifiedHeader: " + _reply->header(QNetworkRequest::LastModifiedHeader).toString());
        list << QString("CookieHeader: " + _reply->header(QNetworkRequest::CookieHeader).toString());
        list << QString("SetCookieHeader: " + _reply->header(QNetworkRequest::SetCookieHeader).toString());
        list << QString("ContentDispositionHeader: " + _reply->header(QNetworkRequest::ContentDispositionHeader).toString());
        list << QString("UserAgentHeader: " + _reply->header(QNetworkRequest::UserAgentHeader).toString());
        list << QString("ServerHeader: " + _reply->header(QNetworkRequest::ServerHeader).toString());
    }
    return list;
}

QPixmap Response::pixmap()
{
    QPixmap pixmap;

    if(_reply->header(QNetworkRequest::ContentTypeHeader).toString().contains("image/jpeg"))
        pixmap.loadFromData(data(), "jpeg");
    else if(_reply->header(QNetworkRequest::ContentTypeHeader).toString().contains("image/png"))
        pixmap.loadFromData(data(), "png");

    return pixmap;
}
