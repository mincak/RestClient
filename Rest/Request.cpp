#include "Request.h"
#include <QDebug>

Request::Request(QObject *parent) : QObject(parent)
{
    _mgr = new QNetworkAccessManager(this);

    connect(_mgr, SIGNAL(finished(QNetworkReply*)), SLOT(requestFinished(QNetworkReply*)));
}

void Request::run(const QString &type, QString url, const QMap<QString, QString> &inputMap)
{
    const QString &input = inputMapToByteArray(inputMap);
    QNetworkRequest request;

    // set some headers
    request.setRawHeader("User-Agent", "RestClient");
    request.setRawHeader("Last-Modified", QDateTime::currentDateTime().toString("HHmmss").toLatin1());
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    qDebug() << "Request headers:" << request.rawHeaderList();

    if(type == "GET" || type == "HEAD") {
        if(!input.isEmpty())
            url.append(QString("?%1").arg(input));
        qDebug() << "Request:" << type << url;
        request.setUrl(url);
        type=="GET" ? _mgr->get(request) : _mgr->head(request);
    }
    else if(type == "POST") {
        request.setUrl(url);
        _mgr->post(request, input.toLatin1());
    }
    else if(type == "PUT") {
        request.setUrl(url);
        _mgr->put(request, input.toLatin1());
    }
    else if(type == "DELETE") {
        request.setUrl(url);
        _mgr->deleteResource(request);
    }
}

void Request::requestFinished(QNetworkReply *reply)
{
    if(_response)
        delete _response;
    _response = new Response(reply, this);

    emit response(_response);
}

// help function for transforming input parameters from map to string for the request
QString Request::inputMapToByteArray(const QMap<QString, QString> &inputMap)
{
    QString res;

    if(inputMap.isEmpty())
        return QString();

    bool first = true;
    foreach(QString key, inputMap.keys()) {
        if(first)
            first = false;
        else
            res.append("&");

        res.append(QUrl::toPercentEncoding(key));
        res.append("=");
        res.append(QUrl::toPercentEncoding(inputMap.value(key)));
    }

    return res;
}
