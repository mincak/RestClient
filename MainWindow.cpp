#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _request = new Request(this);

    connect(ui->pushButton, SIGNAL(clicked(bool)), SLOT(buttonClicked()));
    connect(_request, SIGNAL(response(Response*)), SLOT(showResponse(Response*)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::buttonClicked()
{
    QMap<QString, QString> inputMap;

    inputMap.insert("key1", "value1");
    inputMap.insert("key2", "value2");
    inputMap.insert("key3", "value3");

    _request->run(ui->type->currentText(), ui->address->text(), inputMap);
}

void MainWindow::showResponse(Response *response)
{
    // show error or data from response
    if(response->hasError())
        ui->result->setPlainText(response->error());
    else {
        ui->result->setPlainText(response->data());

        // display image, if there is any
        QPixmap pixmap = response->pixmap();
        if(!pixmap.isNull())
            ui->image->setPixmap(pixmap);

        // show all known headers
        QString headers;
        foreach (QString header, response->headers())
            if(header.split(": ", QString::SkipEmptyParts).size() > 1)
                headers.append(QString("%1\n").arg(header));
        ui->headers->setPlainText(headers);
    }
}
