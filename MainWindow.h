#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Rest/Request.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void buttonClicked();
    void showResponse(Response *response);

private:
    Ui::MainWindow *ui;
    Request *_request;
};

#endif // MAINWINDOW_H
